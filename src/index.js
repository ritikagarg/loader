import React, {useState} from 'react';
import { View, TouchableOpacity, StyleSheet, Text, Platform } from "react-native";
import Constants from './constants';
import axios from 'axios';
import DocumentPicker from 'react-native-document-picker';

const Src = () => {
  const [idFront, setIdFront] = useState(null);

  const uploadDocs = async (name) => {
    const token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOi8vYXBwLmhpc2EuY28vYXBpL2xvZ2luIiwiaWF0IjoxNjM3MDUzODU5LCJleHAiOjE2MzcwNTQ0NTksIm5iZiI6MTYzNzA1Mzg1OSwianRpIjoia01iYk9PeHNJZXlHbWVRWCIsInN1YiI6NTYsInBydiI6Ijg3ZTBhZjFlZjlmZDE1ODEyZmRlYzk3MTUzYTE0ZTBiMDQ3NTQ2YWEifQ.lPnLJ3MMWUBR2YG-7y-mhQuUMyc2vuw6QilUJOVtoyo";
    try {
      const res = await DocumentPicker.pickSingle({
        type: [DocumentPicker.types.allFiles],
      })
      console.log(res)
      const form = new FormData();
      let file = {
        name: res.name,
        type: res.type,
        uri: res.uri.replace("content:", "file:") 
      }
      form.append('file[]', file);
      console.log(form);
    //   const apiResponse = await axios.post("https://app.hisa.co/api/uploadFiles", form, {
    //   headers: {
    //     "Content-Type": "multipart/form-data",
    //     "Authorization": `bearer ${token}`
    //   },
    // })
    // console.log("api response", apiResponse);

    } catch (err) {
      if (DocumentPicker.isCancel(err)) {
        console.log("user cancelled");
        // User cancelled the picker, exit any dialogs or menus and move on
      } else {
        console.log("Unknown error" + JSON.stringify(err));
      }
    }
  }

  return(
    <View>
      <View style={styles.inputcontainer}>
        <Text style={{fontSize:18,marginLeft: 5}}>ID Scan (Front)</Text>
        <TouchableOpacity 
        style={styles.buttonContainer}
        onPress={()=> uploadDocs("id_front")}
        activeOpacity={0.7}
        >
          <Text style={{fontSize:18}}>UPLOAD</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.inputcontainer}>
        <Text style={{fontSize:18,marginLeft: 5}}>ID Scan (Back)</Text>
        <TouchableOpacity 
        style={styles.buttonContainer}
        onPress={()=> uploadDocs("id_back")}
        activeOpacity={0.7}
        >
          <Text style={{fontSize:18}}>UPLOAD</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.inputcontainer}>
        <Text style={{fontSize:18,marginLeft: 5}}>Tax Document</Text>
        <TouchableOpacity 
        style={styles.buttonContainer}
        onPress={()=> uploadDocs("tax_doc")}
        activeOpacity={0.7}
        >
          <Text style={{fontSize:18}}>UPLOAD</Text>
        </TouchableOpacity>
      </View>
      <View style={styles.inputcontainer}>
        <Text style={{fontSize:18,marginLeft: 5}}>Utility Bill</Text>
        <TouchableOpacity 
        style={styles.buttonContainer}
        onPress={()=> uploadDocs("bill")}
        activeOpacity={0.7}
        >
          <Text style={{fontSize:18}}>UPLOAD</Text>
        </TouchableOpacity>
      </View>
    </View>
  )
};

const styles = StyleSheet.create({
  inputcontainer: {
    flexDirection:'row',
    justifyContent: 'space-between',
    height: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 10,
    borderWidth: 1,
    borderColor: 'rgba(33,33,33,0.4)',
    marginTop: Constants.BaseStyle.DEVICE_HEIGHT / 100 * 3,
    borderRadius: 5
},
buttonContainer: {
  width:'30%', 
  margin: 5, 
  borderRadius:5, 
  backgroundColor: 'grey', 
  justifyContent:'center',
  alignItems:'center'}
})

export default Src;